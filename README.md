# Projeto Arduino - Sensor de Distância

## Introdução

O seguinte projeto tem como objetivo elaborar um sensor de distância com um arduino uno. Com base na distância do objeto capturado pelo sensor ultrassônico, o arduino irá acender diferentes LEDs para indicar essa distância. Quando o objeto está a uma distância maior que 50 cm ele ligará a LED verde, para distâncias entre 50 e 25 será ligada a LED amarela, e por fim, para distâncias menores que 25 cm será acendido o LED vermelho.

## Alunos

| Nome        | Número USP     |
| ------------- |:-------------:|
| Marcus Vinícius Teixeira Huziwara     | 11834432 | 
|   Antônio Rodrigues Rigolino   |    11795791   | 
| Enzo Key Yamashita |  13678630  | 
| Alessandro Rodrigues Pereira da Silva |  15436838  | 

## Disciplina

SSC0180 - Eletrônica para Computação

##  Professor

Eduardo do Valle Simões

## Lista dos Componentes

| Quantidade    | Nome     | Custo da unidade |
| ------------- |:-------------:|:-------------:|
| 1 | Arduino UNO | R$ 109.00 | 
| 1 | Sensor Ultrassônico HC-SR04 | R$ 0.95 | 
| 3 | LED | R$ 0.20 | 
| 3 | Resistor 1kΩ 1/4W 5% | R$ 0.10 | 
| 1 | Protoboard  | R$ 11.90 |
| 1 | Kit Jumpers | R$ 25.00 |  

## Descrição dos Componentes

#### Arduino UNO

O Arduino UNO é uma placa de desenvolvimento utilizada para prototipagem rápida, projetos de automação, controle de dispositivos, entre outros.

#### Sensor Ultrassônico

O sensor ultrassônico HC-SR04 é usado para medir distâncias entre o sensor e um objeto, utilizando ondas de som. O sensor emite um sinal ultrassônico e calcula o tempo que leva para o eco retornar ao sensor, permitindo calcular a distância até o objeto.

#### LED

LEDs são diodos emissores de luz que convertem energia elétrica em luz. 

#### Resistores

Resistores são componentes passivos que limitam a corrente elétrica em um circuito.

#### Protoboard

A protoboard é uma placa usada para construir protótipos de circuitos eletrônicos sem a necessidade de solda.

#### Jumpers

Os jumpers são fios de conexão usados para conectar componentes em uma protoboard.

## Circuito no Tinkercard

![circuito](imagens/tinkercard_circuito.png "Circuito Tinkercard")

Link para o [circuito](https://www.tinkercad.com/things/aOfws3hDwXQ-sensor-de-distancia)

## Código executado

O código a seguir na linguagem C++ resgata as distâncias medidas pelo sensor ultrassônico em centimetros e a partir disso define qual LED acender. A leitura do sensor foi feita como a biblioteca externa [Ultrasonic](https://github.com/MakerHero/Ultrasonic).

```c++

#include "Ultrasonic.h"
Ultrasonic ultrasonic(8, 9); // Trigger na porta 8 e Echo na porta 9

const int ledVerde = 10;
const int ledAmarelo = 11;
const int ledVermelho = 12;

long microsec = 0; // variaveis de controle
float distanciaCM = 0;

void setup() {
  Serial.begin(9600); //Inicializando o serial monitor
  pinMode(ledVerde, OUTPUT); //declarando os LEDs como saida
  pinMode(ledAmarelo, OUTPUT);
  pinMode(ledVermelho, OUTPUT);
}

void loop() {
  //Lendo o valor do sensor
  microsec = ultrasonic.timing();

  //Convertendo a distância em CM
  distanciaCM = ultrasonic.convert(microsec, Ultrasonic::CM);

  ledDistancia();

  Serial.print(distanciaCM);// mostrar a distancia na porta serial
  Serial.println(" cm");// colocar unidade de medida
  delay(500);// espera de 500 milissegundos
}

void ledDistancia() {

  //Desliga todos os LEDs
  digitalWrite(ledVerde, LOW);
  digitalWrite(ledAmarelo, LOW);
  digitalWrite(ledVermelho, LOW);

  // cse a distancia for maior que 50 cm
  if (distanciaCM > 50) {
    digitalWrite(ledVerde, HIGH); //liga o LED verde
  }
  // se a distancia for entre 50 cm e 25 cm
  else if (distanciaCM > 25) {
    digitalWrite(ledAmarelo, HIGH); //liga LED amarelo
  }
  else { // se a distancia for menor que 25 cm
    digitalWrite(ledVermelho, HIGH); // liga LED vermelho
  }
}
```

## Vídeo do Funcionamento

<p align="center">
  <img src="imagens/projeto.jpg" alt="Foto Projeto" title="Foto Projeto" width="600"/>
</p>

[Link para o vídeo](https://youtu.be/k-UcYowsh4o)

## Considerações Finais

No contexto do curso de Eletrônica, realizar o projeto com Arduino e sensor ultrassônico foi especialmente interessante. Esse trabalho proporcionou uma oportunidade única de aplicar os conceitos teóricos de eletrônica a uma aplicação prática e inovadora.

